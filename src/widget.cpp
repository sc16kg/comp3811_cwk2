#include "widget.h"
//  -- -- -- -- -- -- WIDGET -- -- -- -- -- -- -- --

typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;


static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0
};

static const float RAD = 3.141592/180;

Widget::Widget(QWidget *parent)
    : QGLWidget(parent),
      _sun("./textures/sun.ppm"),
      _mercury("./textures/mercury.ppm"),
      _venus("./textures/venus.ppm"),
      _earth("./textures/earth.ppm"),
      _mars("./textures/mars.ppm"),
      _jupiter("./textures/jupiter.ppm"),
      _saturn("./textures/saturn.ppm"),
      _uranus("./textures/uranus.ppm"),
      _neptune("./textures/Moi.ppm")
    { // Constructor
      this->zoom = 0.01;
      this->orbit_speed = 1;
      	QString focus;
    } // Constructor

void Widget::initializeGL()
	{ // initializeGL()
    glEnable(GL_TEXTURE_2D);
    glClearColor(0., 0., 0., 0.);
    glColor3f(1.0, 1.0, 1.0);

    textures = new GLuint[9];
    glGenTextures(9,textures);
    for(int i = 0;i<9;i++){
      glBindTexture(GL_TEXTURE_2D, textures[i]);
      if(i == 0){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _sun.Width(), _sun.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _sun.imageField());
      }
      if(i == 1){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _mercury.Width(), _mercury.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _mercury.imageField());
      }
      if(i == 2){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _venus.Width(), _venus.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _venus.imageField());
      }
      if(i == 3){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _earth.Width(), _earth.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _earth.imageField());
      }
      if(i == 4){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _mars.Width(), _mars.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _mars.imageField());
      }
      if(i == 5){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _jupiter.Width(), _jupiter.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _jupiter.imageField());
      }
      if(i == 6){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _saturn.Width(), _saturn.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _saturn.imageField());
      }
      if(i == 7){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _uranus.Width(), _uranus.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _uranus.imageField());
      }
      if(i == 8){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _neptune.Width(), _neptune.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _neptune.imageField());
      }
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    }
	} // initializeGL()

void Widget::resizeGL(int w, int h)
	{ // resizeGL()
    this->zoom = 0.01;
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-4.0, 4.0, -4.0, 4.0, -4.0, 4.0);
	} // resizeGL()

void Widget::orbit(int r){
  materialStruct* p_front = &whiteShinyMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  float x0,x1,z0,z1;
  for(int i=0;i<1000;i++)
  {
    x0= r * cos(i*2*RAD);
    x1= r * cos((i+1)*2*RAD);
    z0= r * sin(i*2*RAD);
    z1= r * sin((i+1)*2*RAD);
    
    glBegin(GL_POLYGON);
    glVertex3f(x0,-0.05,z0);
    glNormal3f(x0,z0,0);
    glVertex3f(x1,-0.05,z1);
    glNormal3f(x1,0,z1);
    glVertex3f(x1,0.05,z1);
    glNormal3f(x1,0,z1);
    glVertex3f(x0,0.05,z0);
    glNormal3f(x0,0,z0);
    glEnd();
    }
}

const int NR_PHI   = 30;
const int NR_THETA = 30;

void Widget::sphere(){
  for(int longitude = 0; longitude < NR_PHI; longitude++)
    for(int latitude = 0; latitude < NR_THETA; latitude++){
      float d_phi   = 2*M_PI/NR_PHI;
      float d_theta = M_PI/NR_THETA;
      glBegin(GL_TRIANGLES);
      double x, y, z;

      x = cos(longitude*d_phi)*sin(latitude*d_theta);
      y = sin(longitude*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude)/NR_THETA);
      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin(latitude*d_theta);
      y = sin((longitude+1)*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(d_phi,0);
      glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude)/NR_THETA);
      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);
      glVertex3f(x, y, z);
      x = cos(longitude*d_phi)*sin(latitude*d_theta);
      y = sin(longitude*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude)/NR_THETA);
      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);
      glVertex3f(x, y, z);
      x = cos((longitude)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);
      glVertex3f(x, y, z);

      glEnd();
  }
}

void Widget::set_zoom(int i){
  float tmp = i;
  tmp = tmp / 100;
  this->zoom = tmp;
  this->update();
}

void Widget::increment_angle(){
  this->angle = (this->angle-0.005*orbit_speed);
  this->update();
}

void Widget::set_orbit_speed(int i){
  float tmp = i;
  tmp = tmp / 100;
  this->orbit_speed = i;
  this->update();
}

void Widget::set_focus(QString input){
  this->focus = input;
}

void Widget::paintGL()
	{ // paintGL()
  float x, z;
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_NORMALIZE);

  glScalef(this->zoom,this->zoom,this->zoom);

  if(this->focus == mercury){
    x = 7 *cos(this->angle*4.15*RAD);
    z = 7 *sin(this->angle*4.15*RAD);
    glTranslatef(-x,0.,-z);
  }
  if(this->focus == venus){
    x = 10 *cos(this->angle*-1*1.62*RAD);
    z = 10 *sin(this->angle*-1*1.62*RAD);
    glTranslatef(-x,0.,-z);
  }
  if(this->focus == earth){
    x = 12 *cos(this->angle*RAD);
    z = 12 *sin(this->angle*RAD);
    glTranslatef(-x,0.,-z);
  }
  if(this->focus == mars){
    x = 17 *cos(this->angle*0.52*RAD);
    z = 17 *sin(this->angle*0.52*RAD);
    glTranslatef(-x,0.,-z);
  }
  if(this->focus == jupiter){
    x = 47 *cos(this->angle*0.08*RAD);
    z = 47 *sin(this->angle*0.08*RAD);
    glTranslatef(-x,0.,-z);
  }
  if(this->focus == saturn){
    x = 84 *cos(this->angle*0.03*RAD);
    z = 84 *sin(this->angle*0.03*RAD);
    glTranslatef(-x,0.,-z);
  }
  if(this->focus == uranus){
    x = 164 *cos(this->angle*-1*0.012*RAD);
    z = 164 *sin(this->angle*-1*0.012*RAD);
    glTranslatef(-x,0.,-z);
  }
  if(this->focus == neptune){
    x = 250 *cos(this->angle*0.006*RAD);
    z = 250 *sin(this->angle*0.006*RAD);
    glTranslatef(-x,0.,-z);
  }
  
  // Sun
  glPushMatrix();
  glScalef(5.,5.,5.);
  glRotatef(90,1.,0.,0.);
  glRotatef(this->angle*13.51,0.,0.,1.);
  glBindTexture(GL_TEXTURE_2D, textures[0]);
  GLfloat light_pos[] = {0., 0., 0., 1.};
  glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
  glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,180.);
  this->sphere();
  glPopMatrix();

  // Mercury
  x = 7 *cos(this->angle*4.15*RAD);
  z = 7 *sin(this->angle*4.15*RAD);
  glBindTexture(GL_TEXTURE_2D, textures[8]);
  this->orbit(7);
  glPushMatrix();
  glTranslatef(x,0.,z);
  glRotatef(90,1.,0.,0.);
  glRotatef(this->angle*6.2,0.,1.,0.);
  glBindTexture(GL_TEXTURE_2D, textures[1]);
  glScalef(0.38,0.38,0.38);
  this->sphere();
  glPopMatrix();

  // Venus
  x = 10 *cos(this->angle*-1*1.62*RAD);
  z = 10 *sin(this->angle*-1*1.62*RAD);
  glBindTexture(GL_TEXTURE_2D, textures[8]);
  this->orbit(10);
  glPushMatrix();
  glTranslatef(x,0.,z);
  glRotatef(90,1.,0.,0.);
  glRotatef(this->angle*-1*1.5,0.,0.,1.);
  glBindTexture(GL_TEXTURE_2D, textures[2]);
  glScalef(0.95,0.95,0.95);
  this->sphere();
  glPopMatrix();

  // Earth
  x = 12 *cos(this->angle*RAD);
  z = 12 *sin(this->angle*RAD);
  glBindTexture(GL_TEXTURE_2D, textures[8]);
  this->orbit(12);
  glPushMatrix();
  glTranslatef(x,0.,z);
  glRotatef(90,1.,0.,0.);
  glRotatef(this->angle*1*365,0.,0.,1.);
  glBindTexture(GL_TEXTURE_2D, textures[3]);
  this->sphere();
  glPopMatrix();

  // Mars
  x = 17 *cos(this->angle*0.52*RAD);
  z = 17 *sin(this->angle*0.52*RAD);
  glBindTexture(GL_TEXTURE_2D, textures[8]);
  this->orbit(17);
  glPushMatrix();
  glTranslatef(x,0.,z);
  glRotatef(90,1.,0.,0.);
  glRotatef(this->angle*350,0.,0.,1.);
  glBindTexture(GL_TEXTURE_2D, textures[4]);
  glScalef(0.5,0.5,0.5);
  this->sphere();
  glPopMatrix();

  // Jupiter
  x = 47 *cos(this->angle*0.08*RAD);
  z = 47 *sin(this->angle*0.08*RAD);
  glBindTexture(GL_TEXTURE_2D, textures[8]);
  this->orbit(47);
  glPushMatrix();
  glTranslatef(x,0.,z);
  glRotatef(90,1.,0.,0.);
  glRotatef(this->angle*876,0.,0.,1.);
  glBindTexture(GL_TEXTURE_2D, textures[5]);
  glScalef(11.9,11.9,11.9);
  this->sphere();
  glPopMatrix();

  // Saturn
  x = 84 *cos(this->angle*0.03*RAD);
  z = 84 *sin(this->angle*0.03*RAD);
  glBindTexture(GL_TEXTURE_2D, textures[8]);
  this->orbit(84);
  glPushMatrix();
  glTranslatef(x,0.,z);
  glRotatef(90,1.,0.,0.);
  glRotatef(this->angle*796,0.,0.,1.);
  glBindTexture(GL_TEXTURE_2D, textures[6]);
  glScalef(9.4,9.4,9.4);
  this->sphere();
  glPopMatrix();

  // Uranus
  x = 164 *cos(this->angle*-1*0.012*RAD);
  z = 164 *sin(this->angle*-1*0.012*RAD);
  glBindTexture(GL_TEXTURE_2D, textures[8]);
  this->orbit(164);
  glPushMatrix();
  glTranslatef(x,0.,z);
  glRotatef(90,1.,0.,0.);
  glRotatef(this->angle*-1*515,0.,0.,1.);
  glBindTexture(GL_TEXTURE_2D, textures[7]);
  glScalef(4.,4.,4.);
  this->sphere();
  glPopMatrix();

  // Neptune
  x = 250 *cos(this->angle*0.006*RAD);
  z = 250 *sin(this->angle*0.006*RAD);
  glBindTexture(GL_TEXTURE_2D, textures[8]);
  this->orbit(250);
  glPushMatrix();
  glTranslatef(x,0.,z);
  glRotatef(this->angle*547.5,0.,0.,1.);
  glScalef(3.88,3.88,3.88);
  this->sphere();
  glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

  gluLookAt(1.0,1.0,1.0, 0.0,0.0,0.0, 0.0,1.0,0.0);

	glFlush();
	} // paintGL()
