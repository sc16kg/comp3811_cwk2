#include "window.h"

//  -- -- -- -- -- -- WINDOW -- -- -- -- -- -- -- --
Window::Window(QWidget *parent)
	: QWidget(parent)
	{ // constructor
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
	topLayout = new QBoxLayout(QBoxLayout::LeftToRight);
	bottomLayout = new QBoxLayout(QBoxLayout::LeftToRight);

	// Widget
	widget = new Widget(this);
	windowLayout->addWidget(widget);
	
	// Labels
	zoomLabel = new QLabel("Zoom           ");
	topLayout->addWidget(zoomLabel);
	speedLabel = new QLabel("Orbit Speed");
	bottomLayout->addWidget(speedLabel);

	// Sliders
	zoomSlider = new QSlider(Qt::Horizontal);
	topLayout->addWidget(zoomSlider);
	zoomSlider->setRange(1,9000);
	connect(zoomSlider, &QSlider::valueChanged, widget, &Widget::set_zoom);
	orbitSpeedSlider = new QSlider(Qt::Horizontal);
	bottomLayout->addWidget(orbitSpeedSlider);
	orbitSpeedSlider->setRange(1,2000);
	connect(orbitSpeedSlider, &QSlider::valueChanged, widget, &Widget::set_orbit_speed);

	// Entry
	planet_selector = new QLineEdit();
	planet_selector->setPlaceholderText("Enter Planet Name (e.g Earth): ");
	connect(planet_selector, &QLineEdit::textChanged, widget, &Widget::set_focus);	


	windowLayout->addLayout(topLayout);
	windowLayout->addLayout(bottomLayout);
	windowLayout->addWidget(planet_selector);
	// Timers
	QTimer *timer = new QTimer(this);
	connect(timer, &QTimer::timeout, widget, &Widget::increment_angle);
	timer->start(10);
	}

Window::~Window()
	{ // destructor
	delete zoomSlider;
	delete widget;
	delete windowLayout;
	} // destructor

void Window::ResetInterface()
	{ 
	widget->update();
	update();
	} 

//  -- -- -- -- -- -- END WINDOW -- -- -- -- -- -- -- --