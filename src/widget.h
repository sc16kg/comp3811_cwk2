#include <QApplication>
#include <QVBoxLayout>
#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QBoxLayout>
#include <QGLWidget>
#include <QString>

#include <math.h>
#include <iostream>

#include "Image.h"
#include <GL/glu.h>
#include <GL/freeglut.h>

#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

class Widget: public QGLWidget
	{ //
	public:
	Widget(QWidget *parent);
	float zoom;
	float angle;
	float orbit_speed;
	QString focus;
	QString *mercury = new QString("Mercury");
	QString *venus = new QString("Venus");
	QString *earth = new QString("Earth");
	QString *mars = new QString("Mars");
	QString *jupiter = new QString("Jupiter");
	QString *saturn = new QString("Saturn");
	QString *uranus = new QString("Uranus");
	QString *neptune = new QString("Neptune");
	Image _sun;
	Image _mercury;
	Image _venus;
	Image _earth;
	Image _mars;
	Image _jupiter;
	Image _saturn;
	Image _uranus;
	Image _neptune;

	public slots:
	void set_zoom(int i);
	void increment_angle();
	void set_orbit_speed(int i);
	void set_focus(QString input);
	GLuint *textures;

	protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	private:
	void sphere();
	void orbit(int r);
	};

#endif
