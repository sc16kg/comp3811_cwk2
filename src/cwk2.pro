TEMPLATE = app
TARGET = cwk2
INCLUDEPATH += . /opt/local/include

QT += widgets opengl gui

LIBS += -lglut -lGL -lGLU

HEADERS += widget.h window.h Image.h
SOURCES += cwk2.cpp \
            widget.cpp \
            window.cpp \
            Image.cpp