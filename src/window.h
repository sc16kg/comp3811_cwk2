#include "widget.h"
#include <QTimer>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLineEdit>
#include <QLabel>
#include <QString>

#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

class Window: public QWidget
{
  public:
  Window(QWidget *parent);
  ~Window();

	QBoxLayout *windowLayout;
  QBoxLayout *topLayout;
  QBoxLayout *bottomLayout;

	Widget *widget;
  QTimer *timer;
	QSlider *zoomSlider;
  QSlider *orbitSpeedSlider;
  QLabel *zoomLabel;
  QLabel *speedLabel;
  QLineEdit *planet_selector;
  QString entry;

  void ResetInterface();
  void Get_Text();


};


#endif
